# Benutzerdefinierte Labels für TalkBack
TalkBack ist eine Android App, die es blinden Menschen ermöglicht zu hören, was
sich auf ihrem Bildschirm befindet.

Es ist zwar möglich komplett barrierefreie Android-Apps zu schreiben, in dem
UI-Elemente mit Labels versehen werden, vile Apps werden aber leider nicht so
entwickelt.

Hierfür bietet TalkBack die Möglichkeit Benutzerdefinierte Labels zu erstellen.
Über das Lokale Kontextmenü können diese hinzugefügt, geändert oder gelöscht
werden.

Außerdem bietet TalkBack die möglichkeit die benutzerdefinierten Labels zu
importieren und zu exportieren.

Dieses Projekt stellt benutzerdefinierte Labels für verschiedene Apps und in
verschiedenen Sprachen bereit um blinden Menschen zu ermöglichen all die
'unbenannten Schaltflächen' da draußen zu erkennen.